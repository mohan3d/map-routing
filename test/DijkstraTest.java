package test;

import utils.HelperMethods;
import datastructures.Graph;
import dijkstra.*;
import org.junit.Before;
import org.junit.Test;


import static org.junit.Assert.*;

public class DijkstraTest {

    public Dijkstra d;
    public Graph g;

    public void testDijkstraOutput() throws Exception{
        assertEquals(d.getShortestPath(g, 0, 5), 6273.99, 0.01);
        assertEquals(d.getShortestPath(g, 1, 2), 640.312, 0.01);
        assertEquals(d.getShortestPath(g, 1, 5), 4376.62, 0.01);
        assertEquals(d.getShortestPath(g, 4, 3), 5245.91, 0.01);
        assertEquals(d.getShortestPath(g, 5, 2), 3736.31, 0.01);
    }

    @Before
    public void initialize(){
        g = HelperMethods.createGraph1();
    }

    @Test
    public void UnoptimizedDijkstraTest() throws Exception {
        d = new UnoptimizedDijkstra();
        testDijkstraOutput();
    }

    @Test
    public void OptimizedDijkstraTest() throws Exception {
        d = new OptimizedDijkstra();
        testDijkstraOutput();
    }

    @Test
    public void BHDijkstraTest() throws Exception {
        d = new BHDijkstra();
        testDijkstraOutput();
    }

    @Test
    public void FHDijkstraTest() throws Exception {
        d = new FHDijkstra();
        testDijkstraOutput();
    }

    @Test
    public void AStarTest() throws Exception {
        d = new AStar();
        testDijkstraOutput();
    }
}
