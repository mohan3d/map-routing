package utils;

import datastructures.Graph;
import datastructures.Vertex;

public class HelperMethods {
    public static double calculateDistance(Vertex from, Vertex to){

        double deltaX = from.getX() - to.getX();
        double deltaY = from.getY() - to.getY();

        return Math.sqrt((deltaX * deltaX) + (deltaY * deltaY));
    }

    public static Graph createGraph1(){
        final int verticesCount = 6;

        Vertex v0 = new Vertex(1000, 2400);
        Vertex v1 = new Vertex(2800, 3000);
        Vertex v2 = new Vertex(2400, 2500);
        Vertex v3 = new Vertex(4000, 0);
        Vertex v4 = new Vertex(4500, 3800);
        Vertex v5 = new Vertex(6000, 1500);

        Graph g = new Graph(verticesCount);

        g.addVertex(v0);
        g.addVertex(v1);
        g.addVertex(v2);
        g.addVertex(v3);
        g.addVertex(v4);
        g.addVertex(v5);

        g.addEdge(0, 1);
        g.addEdge(0, 3);
        g.addEdge(1, 2);
        g.addEdge(1, 4);
        g.addEdge(2, 4);
        g.addEdge(2, 3);
        g.addEdge(2, 5);
        g.addEdge(3, 5);
        g.addEdge(4, 5);

        return g;
    }
}
