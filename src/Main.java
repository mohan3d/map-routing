import datastructures.Graph;
import dijkstra.*;
import utils.HelperMethods;

public class Main {

    public static long calculateTotalTime(Dijkstra d, Graph g, int []sources, int []targets){
        // check sources.length == targets.length;

        long totalTime, startTime, endTime;
        double distance;
        totalTime = 0;

        for (int i = 0; i < sources.length; i++) {

            startTime = System.nanoTime();
            distance = d.getShortestPath(g, sources[i], targets[i]);
            endTime = System.nanoTime();

            System.out.printf("Distance from %d to %d: %f, path ( ", sources[i], targets[i], distance);
            d.printPath(sources[i], targets[i]);
            System.out.printf("), took  %d%n", endTime - startTime);

            totalTime += (endTime - startTime);
        }

        return totalTime;
    }

    public static void main(String[] args) {

        Graph g = HelperMethods.createGraph1();
        int []sources = {0, 1, 1, 4, 5};
        int []targets = {5, 2, 5, 3, 2};

        System.out.printf("%s%nUn-optimized dijkstra's total running time: %d.%n%n",
                "---------------", calculateTotalTime(new UnoptimizedDijkstra(), g, sources, targets));

        System.out.printf("%s%nOptimized dijkstra's total running time: %d.%n%n",
                "---------------", calculateTotalTime(new OptimizedDijkstra(), g, sources, targets));

        System.out.printf("%s%nPriorityQueue(binary heap) dijkstra's total running time: %d.%n%n",
                "---------------", calculateTotalTime(new BHDijkstra(), g, sources, targets));

        System.out.printf("%s%nPriorityQueue(fibonacci heap) dijkstra's total running time: %d.%n%n",
                "---------------", calculateTotalTime(new FHDijkstra(), g, sources, targets));

        System.out.printf("%s%nAStar total running time: %d.%n%n",
                "---------------", calculateTotalTime(new AStar(), g, sources, targets));

    }
}
