package dijkstra;

public class UnoptimizedDijkstra extends Dijkstra {
    protected void initialize(){
        distance[getSource()] = 0;
    }

    protected boolean hasNextVertex(int current){
        return !visited[current];
    }

    protected void updateValues(int currentVertex, int neighbourId, double newDistance) {
        distance[neighbourId] = newDistance;
        parent[neighbourId] = currentVertex;
    }

    protected int getNextVertex(){
        int currentVertex = 1;
        double dist = Double.MAX_VALUE;

        for (int i = 1; i < getVerticesCount(); ++i) {
            if ((!visited[i]) && (dist > distance[i])) {
                dist = distance[i];
                currentVertex = i;
            }
        }

        return currentVertex;
    }
}
