package dijkstra;

import datastructures.FibonacciHeap;

public class FHDijkstra extends PQDijkstra{
    @Override
    public void initializeQueue() {
        pq = new FibonacciHeap<>();
    }
}
