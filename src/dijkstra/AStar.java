package dijkstra;

import datastructures.Pair;
import utils.HelperMethods;

public class AStar extends BHDijkstra{
    private double []heuristic;

    @Override
    protected void initialize() {
        initializeQueue();

        int source = getSource();

        double heuristicCost = HelperMethods.calculateDistance(getVertex(source), getVertex(getTarget()));
        heuristic = new double[getVerticesCount()];
        heuristic[source] = heuristicCost;
        distance[source] = 0;
    }

    @Override
    protected void updateValues(int currentVertex, int neighbourId, double newDistance) {
        double heuristicCost = HelperMethods.calculateDistance(getVertex(neighbourId), getVertex(getTarget()));
        distance[neighbourId] = newDistance;
        parent[neighbourId] = currentVertex;
        heuristic[neighbourId] = distance[neighbourId] + heuristicCost;

        pq.add(new Pair(neighbourId, heuristic[neighbourId]));
    }
}
