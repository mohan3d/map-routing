package dijkstra;

import datastructures.BinaryHeap;

public class BHDijkstra extends PQDijkstra {

    @Override
    public void initializeQueue() {
        pq = new BinaryHeap<>(getVerticesCount());
    }
}
