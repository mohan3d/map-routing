package dijkstra;

import datastructures.Graph;
import datastructures.Pair;
import datastructures.Vertex;

import java.util.ArrayList;
import java.util.Arrays;

public abstract class Dijkstra {
    protected int []parent;
    protected boolean []visited;
    protected double []distance;

    private int source;
    private int target;
    private Graph g;


    public double getShortestPath(Graph g, int source, int target){
        run(g, source, target);
        return distance[target];
    }

    public void printPath(int source, int target){
        if(source == target || target == -1) {
            System.out.print(source + " ");
            return;
        }

        printPath(source, parent[target]);
        System.out.print(target + " ");
    }

    private void initializeSearch(Graph _g, int _source, int _target){
        g = _g;
        source = _source;
        target = _target;

        int verticesCount = g.getVerticesCount();

        parent = new int[verticesCount];
        visited = new boolean[verticesCount];
        distance = new double[verticesCount];

        Arrays.fill(parent, -1);
        // False is the default value for boolean array
        Arrays.fill(distance, Double.MAX_VALUE);
        initialize();
    }

    private void run(Graph _g, int _start, int _target){
        ArrayList<Pair> neighbours;
        int neighbourId, currentVertex;
        double neighbourDistance;

        initializeSearch(_g, _start, _target);
        currentVertex = source;

        while(hasNextVertex(currentVertex)){
            visited[currentVertex] = true;
            neighbours = g.getNeighbours(currentVertex);

            for(Pair neighbour : neighbours){
                neighbourId = neighbour.getId();
                neighbourDistance = neighbour.getDistance();

                if (distance[neighbourId] > (distance[currentVertex] + neighbourDistance)) {
                    updateValues(currentVertex, neighbourId, distance[currentVertex] + neighbourDistance);
                }
            }

            currentVertex = getNextVertex();
        }
    }

    protected int getSource(){
        return source;
    }
    protected int getTarget(){
        return target;
    }
    protected int getVerticesCount(){
        return g.getVerticesCount();
    }
    protected Vertex getVertex(int vertexId){
        return g.getVertex(vertexId);
    }

    protected abstract void initialize();
    protected abstract boolean hasNextVertex(int current);
    protected abstract void updateValues(int currentVertex, int neighbourId, double newDistance);
    protected abstract int getNextVertex();
}