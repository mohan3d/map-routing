package dijkstra;

public class OptimizedDijkstra extends UnoptimizedDijkstra{
    @Override
    protected boolean hasNextVertex(int current) {
        return (!visited[current]) && (current != getTarget());
    }
}
