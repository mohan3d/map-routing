package dijkstra;

import datastructures.Pair;
import java.util.AbstractQueue;

public abstract class PQDijkstra extends OptimizedDijkstra{
    protected AbstractQueue<Pair> pq;

    public abstract void initializeQueue();

    @Override
    protected void initialize() {
        super.initialize();
        initializeQueue();
    }

    @Override
    protected boolean hasNextVertex(int current) {
        if(current == getSource())
            return (!visited[current]) && (current != getTarget());

        return super.hasNextVertex(current) && (!pq.isEmpty());
    }

    @Override
    protected void updateValues(int currentVertex, int neighbourId, double newDistance) {
        super.updateValues(currentVertex, neighbourId, newDistance);
        pq.add(new Pair(neighbourId, distance[neighbourId]));
    }

    @Override
    protected int getNextVertex() {
        return pq.poll().getId();
    }
}
