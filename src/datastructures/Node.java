package datastructures;

public class Node<T extends Comparable<T>> implements Comparable<Node<T>> {
    private T data;
    private Node<T> parent;
    private Node<T> child;
    private Node<T> leftSibling;
    private Node<T> rightSibling;
    private int rank;

    public Node(T data) {
        this(data, null, null, null, null, 0);
        this.leftSibling = this;
        this.rightSibling = this;
    }

    public Node(T data, Node<T> parent) {
        this(data);
        this.parent = parent;
    }

    public Node(T data, Node<T> parent, Node<T> child, Node<T> leftSibling, Node<T> rightSibling,
                int rank) {

        this.data = data;
        this.parent = parent;
        this.child = child;
        this.leftSibling = leftSibling;
        this.rightSibling = rightSibling;
        this.rank = rank;
    }

    public void addChild(Node<T> child) {
        if (this.child != null) {
            child.addSibling(this.child);
        }

        child.parent = this;
        this.child = child;
        ++this.rank;
    }

    public void addSibling(Node<T> sibling) {
        sibling.rightSibling.leftSibling = this;
        this.rightSibling = sibling.rightSibling;
        this.leftSibling = sibling;
        sibling.rightSibling = this;
    }

    public void reset() {
        this.child = null;
        this.leftSibling = null;
        this.rightSibling = null;
        this.parent = null;
    }

    public void setLeftSibling(Node<T> leftSibling) {
        this.leftSibling = leftSibling;
    }

    public void setRightSibling(Node<T> rightSibling) {
        this.rightSibling = rightSibling;
    }

    public void setParent(Node<T> parent) {
        this.parent = parent;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public T getData() {
        return data;
    }

    public Node<T> getParent() {
        return parent;
    }

    public Node<T> getChild() {
        return child;
    }

    public Node<T> getLeftSibling() {
        return leftSibling;
    }

    public Node<T> getRightSibling() {
        return rightSibling;
    }

    public int getRank() {
        return rank;
    }

    @Override
    public int compareTo(Node<T> otherNode) {
        return data.compareTo(otherNode.data);
    }
}
