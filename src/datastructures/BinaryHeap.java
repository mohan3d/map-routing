package datastructures;

import java.util.AbstractQueue;
import java.util.Iterator;

public class BinaryHeap<T extends Comparable<T>> extends AbstractQueue<T>{
    int maxSize;
    int n;
    Object q[];

    private int parentIndex(int n){ return ((n == 1)?(-1):(n/2)); }
    private int childIndex(int n){ return 2 * n; }
    private void swap(int i, int j){
        Object tmp = q[i];
        q[i] = q[j];
        q[j] = tmp;
    }

    @SuppressWarnings("unchecked")
    private void bubble_up(int p){
        if(parentIndex(p) == -1) return;
        if(((T)q[parentIndex(p)]).compareTo((T)q[p]) == 1)
        {
            swap(parentIndex(p), p);
            bubble_up(parentIndex(p));
        }
    }

    @SuppressWarnings("unchecked")
    private void bubble_down(int p){
        int child = childIndex(p);
        int min_index = p;

        for(int i = 0; i <= 1; ++i){
            if((child + i) <= n)
            {
                if((((T) q[min_index]).compareTo(((T)q[child + i]))) == 1) min_index = child + i;
            }
        }

        if(min_index != p){
            swap(min_index, p);
            bubble_down(min_index);
        }
    }

    private void push(T item){
        if(n >= maxSize - 1) extend();

        n++;
        q[n] = item;
        bubble_up(n);
    }

    @SuppressWarnings("unchecked")
    private void pop(){
        assert(n > 0);
        // T min = (T) q[1];
        q[1] = q[n];
        n--;
        bubble_down(1);
    }

    @SuppressWarnings("unchecked")
    private T top(){ return (T) q[1]; }

    private void extend(){
        Object []newArr = new Object[q.length * 2];
        System.arraycopy(q, 0, newArr, 0, q.length);

        q = newArr;
        maxSize *= 2;
    }

    public BinaryHeap(int _maxSize){
        maxSize = _maxSize + 1;
        n = 0;
        q = new Object[maxSize];
    }

    public BinaryHeap(){ this(10); }

    @Override
    public Iterator<T> iterator() {
        return null;
    }

    @Override
    public int size(){
        return n;
    }

    @Override
    public boolean isEmpty(){ return (n == 0); }

    @Override
    public boolean offer(T t) {
        push(t);
        return true;
    }

    @Override
    public T poll() {
        T tmp = top();
        pop();
        return tmp;
    }

    @Override
    public T peek() {
        return top();
    }

    @Override
    public boolean add(T t) {
        return offer(t);
    }
}
