package datastructures;

public class Pair implements Comparable<Pair>{
    private int vertexId;
    private double distance;

    public Pair(int vertexId, Vertex vertex, Vertex otherVertex){
        this(vertexId, vertex.calculateDistance(otherVertex));
    }

    public Pair(int vertexId, double distance) {
        this.vertexId = vertexId;
        this.distance = distance;
    }

    public int getId(){
        return vertexId;
    }

    public double getDistance(){
        return distance;
    }

    @Override
    public int compareTo(Pair o) {
        return Double.compare(this.distance, o.distance);
    }
}
