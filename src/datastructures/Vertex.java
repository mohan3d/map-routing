package datastructures;

import utils.HelperMethods;

public class Vertex {
    private int x;
    private int y;

    public Vertex(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public double calculateDistance(Vertex otherVertex){
        return HelperMethods.calculateDistance(this, otherVertex);
    }

    public int getX() {
        return x;
    }
    public int getY() {
        return y;
    }
}
