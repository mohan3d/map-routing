package datastructures;

import java.util.ArrayList;

public class Graph {
    private ArrayList<Vertex> vertices;
    private ArrayList<ArrayList<Pair>> edges;

    public Graph(int verticesCount) {
        vertices = new ArrayList<>(verticesCount);
        edges = new ArrayList<>(verticesCount);

        for (int i = 0; i < verticesCount; i++) {
            edges.add(new ArrayList<>());
        }
    }

    public int getVerticesCount(){
        return vertices.size();
    }

    public Vertex getVertex(int vertexId){
        return vertices.get(vertexId);
    }

    public ArrayList<Pair> getNeighbours(int vertexId){
        return edges.get(vertexId);
    }

    public void addVertex(Vertex v){
        vertices.add(v);
    }

    public void addEdge(int v1, int v2){
        addDirectedEdge(v1, v2);
        addDirectedEdge(v2, v1);
    }

    public void addDirectedEdge(int source, int target){
        Pair p = new Pair(target, vertices.get(target), vertices.get(source));
        edges.get(source).add(p);
    }
}
