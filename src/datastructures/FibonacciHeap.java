package datastructures;

import java.util.AbstractQueue;
import java.util.Iterator;

public class FibonacciHeap<T extends Comparable<T>> extends AbstractQueue<T> {
    private Node<T> minNode;
    private int _size;

    public FibonacciHeap() {
        minNode = null;
        _size = 0;
    }

    private void addNode(T p) {
        if (minNode == null) {
            minNode = new Node<>(p);
        } else {
            Node<T> newNode = new Node<>(p);
            newNode.addSibling(minNode);

            if (newNode.compareTo(minNode) == -1)
                minNode = newNode;
        }

        ++_size;
    }

    private T deleteMin() {
        if (minNode == null) return null;

        Node tmp = minNode;

        if (minNode == minNode.getRightSibling()) {
            // has no siblings.

            if (minNode.getChild() == null) {
                // has no children.
                minNode = null;
            } else {
                // add all children to root list.
                meldChildren(minNode);
            }
        } else {
            // has siblings.

            if (minNode.getChild() == null) {
                // has no children.

                minNode.getRightSibling().setLeftSibling(minNode.getLeftSibling());
                minNode.getLeftSibling().setRightSibling(minNode.getRightSibling());
                minNode = minNode.getLeftSibling();
            } else {
                // has children.
                meldChildren(minNode);
            }
        }

        // Set new min element.
        updateMin();

        // Reset previous min.
        tmp.reset();

        --_size;

        consolidateTrees();

        return (T) tmp.getData();
    }

    private void meldChildren(Node node) {
        Node child = node.getChild();

        do {
            child.setParent(null);
            child = child.getRightSibling();
        } while (child != node.getChild());


        if (node.getRightSibling() != node) {
            // has siblings.
            child.getRightSibling().setLeftSibling(node.getLeftSibling());
            node.getLeftSibling().setRightSibling(child.getRightSibling());
            child.setRightSibling(node.getRightSibling());
            node.getRightSibling().setLeftSibling(child);
        }

        // temporary set minNode to child.
        minNode = child;
    }

    private void updateMin() {
        if (minNode == null) return;

        Node tmp, nextMin;
        tmp = nextMin = minNode;

        do {
            if (nextMin.compareTo(tmp) == 1)
                nextMin = tmp;

            tmp = tmp.getRightSibling();
        } while (tmp != minNode);

        minNode = nextMin;
    }

    private void consolidateTrees() {
        boolean dirty = true;
        Node<T>[] nodes = new Node[_size + 1];
        Node<T> tmp = null;

        while (tmp != minNode && dirty) {
            dirty = false;
            tmp = minNode;

            do {
                if (nodes[tmp.getRank()] != null) {

                    if (tmp == nodes[tmp.getRank()]) {
                        dirty = false;
                        break;
                    }

                    tmp = combineTrees(tmp, nodes[tmp.getRank()]);
                    nodes[tmp.getRank() - 1] = null;
                    dirty = true;
                }

                nodes[tmp.getRank()] = tmp;
                tmp = tmp.getRightSibling();

            } while (tmp != minNode);
        }
    }

    private Node<T> combineTrees(Node<T> firstNode, Node<T> secondNode) {
        // Always make sure first < second
        // If second < first
        if (secondNode.compareTo(firstNode) == -1) {
            // Swap
            Node<T> tmp = firstNode;
            firstNode = secondNode;
            secondNode = tmp;
        }

        // Delete second from the root list.
        if (secondNode.getRightSibling() == firstNode && secondNode.getLeftSibling() == firstNode) {
            // Only first and second in the root list.
            firstNode.setLeftSibling(firstNode);
            firstNode.setRightSibling(firstNode);

        } else {
            // Root list has more than two childs.
            secondNode.getLeftSibling().setRightSibling(secondNode.getRightSibling());
            secondNode.getRightSibling().setLeftSibling(secondNode.getLeftSibling());
        }

        // Partially reset second (don't remove its child list).
        secondNode.setParent(null);
        secondNode.setRightSibling(secondNode);
        secondNode.setLeftSibling(secondNode);

        // Add second as child to first.
        firstNode.addChild(secondNode);

        return firstNode;
    }

    @Override
    public boolean offer(T t) {
        addNode(t);
        return true;
    }

    @Override
    public T poll() {
        T tmp = peek();
        deleteMin();
        return tmp;
    }

    @Override
    public T peek() {
        return minNode.getData();
    }

    @Override
    public Iterator<T> iterator() {
        return null;
    }

    @Override
    public int size() {
        return _size;
    }

    @Override
    public boolean isEmpty() {
        return size() == 0;
    }
}

