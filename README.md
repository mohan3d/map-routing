# Map-Routing
An optimized shortest path-finding project that implements Dijsktra's algorithm on a map.

**The original repository can be found [here](https://github.com/Mohamed-Ali-Mohamed/Map-Routing).**

# Optimization ideas
* Stop searching after discovering the shortest path from source to destination.
* Using priority queue.
* Implementing A* algorithm.
